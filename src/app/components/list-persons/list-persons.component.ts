import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/models/person';
import { ApiService } from 'src/app/services/api.service';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-list-persons',
  templateUrl: './list-persons.component.html',
  styleUrls: ['./list-persons.component.scss']
})
export class ListPersonsComponent implements OnInit {

  persons: Person[] = [];


  constructor(private personService: PersonService){}

  ngOnInit(): void {
    this.persons=this.personService.getPersons();

  }

}
