import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
  taskCreationForm: FormGroup =new FormGroup({
    titleControl: new FormControl('', [Validators.required]),
    finishControl: new FormControl('', [Validators.required])
  });

  person: Person;
  id: number;

  constructor(private router: Router, private personService: PersonService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // const title: FormControl = this.taskCreationForm.get('titleControl') as FormControl;
    // title.valueChanges.subscribe((value: string)=>{
    //   console.log(value);
    // })
    // const finish: FormControl = this.taskCreationForm.get('finishControl') as FormControl;
    // finish.valueChanges.subscribe((value: string)=>{
    //   console.log(value);
    // })
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      
      this.id = Number(paramMap.get('id'));
      this.person=this.personService.getPersonById(this.id);
      
    })
  }

  submitForm(){
    const titleC: FormControl = this.taskCreationForm.get('titleControl') as FormControl;
    const titleValue : string = titleC.value;
    const finishC: FormControl = this.taskCreationForm.get('finishControl') as FormControl;
    const finishValue: string = finishC.value;
    let finish : boolean;
    if(finishValue==="true"){
      finish=true;
    }
    else{
      finish=false;
    }

    const task: Task = {title: titleValue, done: finish};

    this.personService.addTaskToPeron(this.person.id, task);

    this.router.navigate(['/persons/details', this.person.id]);
  }

}
