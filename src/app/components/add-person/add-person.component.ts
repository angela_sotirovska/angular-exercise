import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.scss']
})
export class AddPersonComponent implements OnInit {

  personCreationForm: FormGroup =new FormGroup({
    nameControl: new FormControl('', [Validators.required]),
    ageControl: new FormControl('', [Validators.required])
  });

  constructor(private router: Router, private personService: PersonService) { }

  ngOnInit(): void {
    const name: FormControl = this.personCreationForm.get('nameControl') as FormControl;
    name.valueChanges.subscribe((value: string)=>{
      console.log(value);
      console.log(name.valid);
    })
    const age: FormControl = this.personCreationForm.get('ageControl') as FormControl;
    age.valueChanges.subscribe((value: string)=>{
      console.log(value);
      console.log(age.valid);
    })
  }

  submitForm(){
    const name: FormControl = this.personCreationForm.get('nameControl') as FormControl;
    const nameValue : string = name.value;
    const age: FormControl = this.personCreationForm.get('ageControl') as FormControl;
    const ageValue: string = age.value;

    this.personService.addPerson(nameValue, ageValue);
    this.router.navigate(['persons']);
  }

  isFormFilled(): boolean{
    const name: FormControl = this.personCreationForm.get('nameControl') as FormControl;
    const nameValue : string = name.value;
    const age: FormControl = this.personCreationForm.get('ageControl') as FormControl;
    const ageValue: string = age.value;

    return nameValue!=="" || ageValue!=="";
  }

}
