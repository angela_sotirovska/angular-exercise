import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  post: any;

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiService){}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      
      const id = Number(paramMap.get('id'));
      this.apiService.getPostById(id).subscribe((data:any[]) => {
        this.post=data;
      });
      
    })
  
  }

}
