import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.scss']
})
export class ListPostsComponent implements OnInit {

  posts: any[] = [];

  post: any = null;

  constructor(private httpClient: HttpClient, private apiService: ApiService){}

  ngOnInit(): void {

    this.getPostsList();
    
  }

  getPostsList(): void{
    this.apiService.getPosts().subscribe((data:any[]) => {
      console.log(data);
      this.posts=data;
    });
  }

  addPost(): void{
    this.post = {userId: 1, id: this.posts.length+1, title: "My post", body: "This is my post!"};
    this.apiService.addPost(this.post).subscribe(()=>{
    });    
  }

}
