import { Component, Input, OnInit } from '@angular/core';
import {Task} from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent {

  @Input()
  task!: Task;

  constructor(private taskService: TaskService){}

  changeDoneValue(){
    this.taskService.changeDoneValue(this.task);
  }

}
