import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Person } from 'src/app/models/person';
import { PersonService } from 'src/app/services/person.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit{

  constructor(private personService: PersonService, private route: ActivatedRoute, private router: Router){}

  c: string="red";
  person: Person;

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      
      const id = Number(paramMap.get('id'));
      this.person=this.getPerson(id);
      
    })
    this.route.queryParamMap.subscribe((paramMap: ParamMap) => {
      const color = paramMap.get('color');
      if(color){
        this.c=color;
      }
      else{
        this.c="black";
      }
    })
  
  }

  getPerson(id: number):Person{
    return this.personService.getPersonById(id);
  }
  

  changeVisibilityOfTasks(person: Person): void{
    this.personService.changeVisibilityOfTasks(person);
  }

  showNumberOfUnfinishedTasks(person: Person):number{
    return this.personService.showNumberOfUnfinishedTasks(person);
  }

  openAddTask(id:number){
    this.router.navigate(['/add-task', id]);
  }
}
