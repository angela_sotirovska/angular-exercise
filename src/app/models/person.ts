import { Task } from "./task";

export interface Person{
    id: number,
    name: string;
    age: number;
    tasks: Array<Task>;
    tasksVisibility: boolean;
}