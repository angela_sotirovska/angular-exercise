import { Component, NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PersonComponent } from './components/person/person.component';
import { AppComponent } from './app.component';
import { ErrorComponent } from './components/error/error.component';
import { ListPersonsComponent } from './components/list-persons/list-persons.component';
import { AddPersonComponent } from './components/add-person/add-person.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { ListPostsComponent } from './components/list-posts/list-posts.component';
import { PostComponent } from './components/post/post.component';
import { CreatingPersonGuard } from './guards/creating-person.guard';

const routes: Route[] = [
  {
    path: 'persons',
    component: ListPersonsComponent,
    children: [
      {
        path:'details/:id',
        component: PersonComponent,
      }
    ]
  },
  {
    path: 'add-task/:id',
    component: AddTaskComponent
  },
  {
    path: 'add-person',
    component: AddPersonComponent,
    canDeactivate: [CreatingPersonGuard]
  },
  {
    path: '',
    redirectTo: 'persons',
    pathMatch: "full"
  },
  {
    path: 'posts',
    component: ListPostsComponent,
  },
  {
    path: 'post/:id',
    component: PostComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
