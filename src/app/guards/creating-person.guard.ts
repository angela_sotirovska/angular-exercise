import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AddPersonComponent } from '../components/add-person/add-person.component';

@Injectable({
  providedIn: 'root'
})
export class CreatingPersonGuard implements CanDeactivate<AddPersonComponent> {
  canDeactivate(
    component: AddPersonComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const isFormFilled: boolean = component.isFormFilled();
      if(isFormFilled){
        return window.confirm("Discard changes?");
      }
    return true;
  }
  
}
