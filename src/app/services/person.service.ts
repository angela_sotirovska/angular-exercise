import { Injectable } from '@angular/core';
import { Person } from '../models/person';
import { Task } from '../models/task';
import { TaskService } from './task.service';

@Injectable()
export class PersonService {


  constructor(private taskService: TaskService) { }

  tasks: Task[] = this.taskService.getTasks();

  private persons: Person[] = [
    {id:0, name: "Maria", age: 25, tasks: [this.tasks[0], this.tasks[3]], tasksVisibility: false},
    {id: 1, name: "Rob", age: 32, tasks: [this.tasks[1], this.tasks[4]], tasksVisibility: false},
    {id: 2, name: "Jennifer", age: 28, tasks: [this.tasks[2], this.tasks[5]], tasksVisibility: false}
  ];

  getPersons(): Person[]{
    return this.persons;
  }

  getPersonById(id:number):Person{
    return this.persons.find((p)=>p.id===id)!;
  }

  showNumberOfUnfinishedTasks(person: Person): number{
    //return person.tasks.length-person.tasks.filter((task) => task.done).length;
    return person.tasks.filter((task)=>!task.done).length;
  }

  changeVisibilityOfTasks(person: Person): void{
    person.tasksVisibility=!person.tasksVisibility;
  }

  addPerson(name: string, age: string):void{
    const idP:number=this.persons.length;
    const tasks: Task[] =[];
    const person: Person = {id: idP, name: name, age: Number(age), tasks: tasks , tasksVisibility: false };
    this.persons.push(person);
  }

  addTaskToPeron(id: number, task: Task): void{
    const person:Person = this.getPersonById(id);
    person.tasks.push(task);
  }
}
