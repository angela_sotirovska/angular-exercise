import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, KeyValueDiffers, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService implements OnInit{

  posts: any[];
  url: string = "https://jsonplaceholder.typicode.com/posts";

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    // const url: string = "https://jsonplaceholder.typicode.com/posts";

    // this.httpClient.get<any[]>(url).subscribe({
    //   next: (posts: any[]) => {
    //     this.posts=posts;
    //   },
    //   error: (httpErrorResponse: HttpErrorResponse) => {
    //     console.log(httpErrorResponse);
    //   }
    // })
  }

  getPosts(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.url);
  }

  addPost(post: any): Observable<any>{
    return this.httpClient.post<any>(this.url, post);
  }

  getPostById(id: number): any{
    return this.httpClient.get<any>(this.url+"/"+id);
  }
}
