import { Injectable } from '@angular/core';
import { Task } from '../models/task';

@Injectable()
export class TaskService {

  private tasks: Task[] = [
    {title:"First Task", done: true},
    {title:"Second Task", done:false},
    {title:"Third Task", done:false},
    {title:"Fourth Task", done: true},
    {title:"Fifth Task", done: true},
    {title:"Sixth Task", done: true}
  ] 

  constructor() { }

  getTasks(): Task[]{
    return this.tasks;
  }

  changeDoneValue(task: Task): void{
    task.done=!task.done;
  }
}
