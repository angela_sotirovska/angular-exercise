import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TaskComponent } from './components/task/task.component';
import { PersonService } from './services/person.service';
import { TaskService } from './services/task.service';
import { PersonComponent } from './components/person/person.component';
import { AppRoutingModule } from './app-routing.module';
import { ErrorComponent } from './components/error/error.component';
import { ListPersonsComponent } from './components/list-persons/list-persons.component';
import { AddPersonComponent } from './components/add-person/add-person.component';

import { AddTaskComponent } from './components/add-task/add-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { ListPostsComponent } from './components/list-posts/list-posts.component';
import { PostComponent } from './components/post/post.component';


@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    PersonComponent,
    ErrorComponent,
    ListPersonsComponent,
    AddPersonComponent,
    AddTaskComponent,
    ListPostsComponent,
    PostComponent
  ],
  imports: [
    BrowserModule, 
    AppRoutingModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [PersonService,
    TaskService, ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
